# Readme for laps
Laps is an open-source implementation of LAPS (Local Administrator Password Solution) for GNU/Linux.

# What does this package do?
This package provides the LAPS client-side functionality. Specifically, those abilities are:
* Update machine root password saved in ldap
* Read attribute, when run as a domain admin user

# Using laps package
The package includes an anacron file for running laps every day. The process will update the password stored in ldap and the user password based on the expiration date on the config file.
See /etc/laps/laps.conf.example for how to configure the client.

The administrator needs to write **/etc/laps/laps.conf** and **/etc/laps/lapsldap.conf**. Copying and modifying the example config files is the recommended way to provide the configs.

For first use, use the -f flag to force the password change so the timestamp is initialized.

    /usr/share/laps/laps.sh -f

# Prepare the domain
The OU where the Linux systems are placed in the domain will need some ACLS set up, which are identical to what the LAPS documentation describes. For a brief summary:

On the OU that contains the Linux computers, use these permissions.
* SELF, applicable to descendant computer objects
  * Write ms-Mcs-AdmPwd
  * Read/Write ms-Mcs-AdmPwdExpirationTime
* <Domain Admins> or other designated group, application to descendant computer objects
  * Read ms-Mcs-AdmPwd
  * Read/Write ms-Mcs-AdmPwdExpirationTime

Systems should be placed in this OU which has the inheritable ACLs.

# Building laps
## Fetch the source
There are 2 options for getting the source
* Clone the git repository

    mkdir -p ~/dev/laps ; cd ~/dev/laps
    git clone https://github.com/bgstack15/laps.git

* Download the tarball 
  * Save it to ~/rpmbuild/SOURCES/

## Building rpms
#### From the git repository
Run the build-laps.sh script which generates the tarball in ~/rpmbuild/SOURCES suitable for building the rpm. This script builds the source tarball with the correct name and runs rpmbuild using the spec file.

    ./build-laps.sh

#### From a tarball
The source tarball includes the rpm spec file. Just run rpmbuild with the right flags.

    rpmbuild -ta laps-0.0.1.tgz

## Building debs
// Not implemented yet

## Installing from a downloaded tarball
The src/ directory in the tarball contains the exact directory structure. The solution consists of a few config files, a few shell and python scripts, and an anacron job file.

# Maintaining this package

# Reference
1. [Microsoft LAPS download page](https://www.microsoft.com/en-us/download/details.aspx?id=46899)
