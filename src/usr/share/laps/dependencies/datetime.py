#!/usr/bin/python2
# File: datetime.py
# Location: /usr/share/laps4linux/dependencies
# Author: bgstack15
# Startdate: 2018-10-17 10:32
# Title: Script that Converts Windows FILETIME to Epoch and Vice-versa
# Purpose: convert timestamps easily
# Package: laps4linux
# Usage: see PARSE PARAMETERS block
# Reference:
#    formula from https://stackoverflow.com/questions/5471379/ways-to-convert-epoch-linux-time-to-windows-time/5471380#5471380
#    python2 stderr https://stackoverflow.com/questions/5574702/how-to-print-to-stderr-in-python
# Improve:
# Dependencies:
#    python2
import sys,argparse

datetimepyverison="2018-10-17a"

# DEFINE FUNCTIONS
def get_epochtime(filetime):
   return int(( filetime - 116444736000000000 ) / 10000000)

def get_filetime(epochtime):
   return int(( epochtime * 10000000 ) + 116444736000000000)

# DEFINE VARIABLES
action = get_epochtime

# example filetime
#filetime = 131859734013606415 

# PARSE PARAMETERS
parser = argparse.ArgumentParser(description="Convert FILETIME to epoch and vice-versa")
f_or_e = parser.add_mutually_exclusive_group()
f_or_e.add_argument("-e","--epoch",action='store_true',help='convert FILETIME to epoch. Default value.')
f_or_e.add_argument("-f","--filetime",action='store_true', help='convert epoch to FILETIME')
parser.add_argument("timestamp",action='store',help='number to convert',nargs='+')
args = parser.parse_args()

if args.filetime:
   action = get_filetime

for timestamp in args.timestamp:
   # for debugging
   #sys.stderr.write("python timestamp:"+str(timestamp)+"\n")
   print action(float(timestamp))
